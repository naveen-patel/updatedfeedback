import express from "express";
import bodyParser from "body-parser";
import { Customer } from "./Routes/customer";
import swaggerUI from "swagger-ui-express";
import swaggerDoc from "./swagger.json";
// import { Product } from "./routes/product";
import cors from "cors";
import path from 'path';
class Application {
  public app: express.Application;
  public routeCustomer: Customer = new Customer();
  // public routeProduct: Product = new Product();


  constructor() {
    this.app = express();
    this.config();
    this.routeCustomer.routes(this.app);
    // this.routeProduct.routes(this.app);
  }
  config() {
    
    let publicDir = path.join(__dirname, '../upload');
    this.app.use(express.static(publicDir));
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use("/swagger", swaggerUI.serve, swaggerUI.setup(swaggerDoc));
    this.app.use(cors());
  }
}

export default new Application().app;
