import app from './app'
import mongoose from 'mongoose'
const PORT=3001

app.listen(PORT,()=>{
    console.log("Server is running feedback")
    mongoose.connect('mongodb://localhost:27017/feedback',{ useNewUrlParser: true})
    .then(()=>console.log("Database Connected for feedback"))
    .catch(err=>console.log("Error is",err))
})
