// for inserting data in category collections
import ScheduleModel from "../Model/schedule"
import { Request, Response } from "express";
/**
* @param category_name
*/
const receiver = (req: Request, res: Response) => {
    ScheduleModel.find({sender_id:req.body._id,flag:false}).select('-_id')
        .then(result => {
          res.status(200).json({status_code:200,success: true,message: "Developers name for feedback", list:result});
        })
   .catch(err => {
      res.status(404).json({status_code:404,success: false,message: "Something went wrong",error_message: err});
    });
  
}  

export default receiver