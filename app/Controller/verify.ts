// for user login

import AdminModel from '../Model/admin'
import { Request, Response } from 'express'
import Joi from "joi";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt'

const verify = (req: Request, res: Response) => {

    let id: number

    const schema = Joi.object().keys({
        passkey: Joi.number().required()
    });
    Joi.validate(req.body, schema, (err: any, value: string) => {
        if (err) {
            return res
                .status(400)
                .json({
                    err,
                    status_code: 400,
                    message: "joi validation error",
                    error_message: err
                });
        }
        else {

            AdminModel.findOne({ passkey: req.body.passkey })
                .then(result => {
                    if (result == null) {
                        return res.status(404).json({ status_code: 404, success: false, message: "passkey not found", error_message: err })
                    }
                    else {
                        jwt.sign({ passkey: result.passkey }, 'secretkey', (err: any, token: string) => {
                            res.status(200).json({ status_code: 200, success: true, message: "admin verification success", token: token, admin:result.name})
                        })
                    }
                })
                .catch(err => {
                    res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                })
        }
    })
}

export default verify