//for registration new user

import CustomerModel from "../Model/customer";
import { Request, Response } from "express";
import Joi from "joi";
import sendmail from 'sendmail'
import ForgotpassModel from "../Model/forgotpass";


const forgotPassword = (req: Request, res: Response) => {

    const emailSender = sendmail({
        silent: false
    });

    const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
        new Promise((resolve, reject) => {
            emailSender(options, (err, reply) => {
                // if error happened or returned code is now started with 2**
                if (err || !reply.startsWith("2")) {
                    reject(err);
                    console.log('error');
                } else {
                    resolve(true);
                    console.log('done');

                }
            });
        });

    //for empty field
    if (req.body === undefined) {
        return res
            .status(404)
            .json({ success: false, message: "Something went wrong" });
    }
    // for validation
    const schema = Joi.object().keys({
        email_id: Joi.string()
            .email({ minDomainAtoms: 2 }).regex(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
            .required()
    });
    Joi.validate(req.body, schema, (err: any, value: string) => {
        if (err) {
            return res
                .status(400)
                .json({
                    err,
                    code: 400,
                    message: "joi validation error",
                    error_message: err
                });
        } else {
            var otparray = ['121212','566765','268900','126789','123567','234234','678678','786878']
            var otp = otparray[Math.floor(Math.random() * otparray.length)];
            let output = `
<h3>Hi user your details are mentioned below:</h3>
<ul>  
  <li>OTP: ${otp}</li>
  <li>For change password please login<button><a href="www.google.com">Visit here</a></button></li>
</ul>`
            CustomerModel.findOne({ email_id: req.body.email_id })
                .then(results => {
                    if (results == null) {
                        return res.status(200).json({ status_code: 404, success: false, message: "Mail not found", error_message: err })
                    }
                    else {
                        ForgotpassModel.deleteOne({email_id: req.body.email_id})
                        .then(result => {
                            let newData = new ForgotpassModel({
                                email_id: req.body.email_id,
                                customer_id: results._id,
                                otp: otp
                            });
                            newData
                                .save()
                                .then(result => {
                                    sendEmail({
                                        from: 'naveenpatel0202@gmail.com',
                                        to: req.body.email_id,
                                        subject: 'OTP for password reset',
                                        html: output,
                                    })
                                    res.status(200).json({ status_code: 200, success: true, message: "email sent on register email ID", result:result});
                                })
                                .catch(err => {
                                    res
                                        .status(202)
                                        .json({
                                            success: false,
                                            status_code: 404,
                                            message: "Something went wrong",
                                            error_message: err
                                        });
                                });
                            })
                            .catch(err => {
                                res
                                    .status(202)
                                    .json({
                                        success: false,
                                        status_code: 404,
                                        message: "Something went wrong",
                                        error_message: err
                                    });
                            });
                       
                    }

                })
                .catch(err => {
                    res.status(200).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                })

        }
    })
}
export default forgotPassword;
