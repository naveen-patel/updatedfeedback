// for inserting data in category collections
import FeedbackModel from "../Model/feedback"
import { Request, Response } from "express";
import ScheduleModel from "../Model/schedule";
/**
* @param category_name
*/
const feedbacksave = (req: Request, res: Response) => {

    ScheduleModel.find({ sender_id: req.body._id, receiver_id: req.body.receiver_id })
        .then(result => {
            console.log(result)
            if (result[0].flag) {
                res.status(200).json({ status_code: 200, success: true, message: "feedback already inserted" });
            }
            else {
                let newData = new FeedbackModel({
                    feedback: req.body.feedback,
                    customer_id: req.body.receiver_id
                });
                newData
                    .save()
                    .then(result => {
                        ScheduleModel.update({ sender_id: req.body._id, receiver_id: req.body.receiver_id }, {
                            flag: true
                        })
                            .then(result => {
                                res.status(200).json({ status_code: 200, success: true, message: "feedback successfully inserted" });


                            })

                    })
            }
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
        });


}

export default feedbacksave