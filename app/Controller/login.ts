// for user login

import CustomerModel from '../Model/customer'
import { Request, Response } from 'express'
import Joi from "joi";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt'

const customerLogin = (req: Request, res: Response) => {

    let id: number

    const schema = Joi.object().keys({
        email_id: Joi.string().email({ minDomainAtoms: 2 }).regex(/^[a-zA-Z]+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
    });
    Joi.validate(req.body, schema, (err: any, value: string) => {
        if (err) {
            return res
                .status(400)
                .json({
                    err,
                    status_code: 400,
                    message: "joi validation error",
                    error_message: err
                });
        } else {

            CustomerModel.findOne({ email_id: req.body.email_id })
                .then(result => {
                    if (result == null) {
                        return res.status(200).json({ status_code: 404, success: false, message: "Mail not found", error_message: err })
                    }
                    else {
                        const { password, ...user } = result.toObject(); //{expiresIn:'30s'},
                        if (bcrypt.compareSync(req.body.password, password)) {
                            jwt.sign({ email_id: result.email_id, id: result._id }, 'secretkey', (err: any, token: string) => {


                                /**
                                * @param email_id
                                * @param password
                                * @returns details of user and cart count
                                */
                                    
                                        res.status(200).json({ status_code: 200, success: true, message: "Successfully login ", token: token})
                                    })

                        }
                        else {
                            res.status(200).json({ status_code: 404, success: false, message: "Password not match", error_message: err })
                        }

                    }

                })
                .catch(err => {
                    res.status(200).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                })
        }
    })
}

export default customerLogin