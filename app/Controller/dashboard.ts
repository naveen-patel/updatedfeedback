// for user dashboard information
import CustomerModel from "../Model/customer";
import { Request, Response } from "express";
import FeedbackModel from "../Model/feedback";
/**
* @returns all product details
*/
const dashboard = (req: Request, res: Response) => {
    console.log('body', req.body)
    CustomerModel.find({ _id: req.body._id })
        .then(results => {
            FeedbackModel.find({ customer_id: req.body._id }).select("-customer_id")
                .then(result => {
                    console.log(results);
                    setTimeout(() => {
                        res.status(200).json({ status_code: 200, success: true, message: "User details", data: results, feedback: result })
                    }, 1000);

                })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default dashboard