//for registration new user
import bcrypt from "bcrypt";
import CustomerModel from "../Model/customer";
import { Request, Response } from "express";
import Joi from "joi";
import sendmail from 'sendmail'
import ForgotpassModel from "../Model/forgotpass";


const setPassword = (req: Request, res: Response) => {

    const emailSender = sendmail({
        silent: false
    });

    const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
        new Promise((resolve, reject) => {
            emailSender(options, (err, reply) => {
                // if error happened or returned code is now started with 2**
                if (err || !reply.startsWith("2")) {
                    reject(err);
                    console.log('error');
                } else {
                    resolve(true);
                    console.log('done');

                }
            });
        });

    //for empty field
    if (req.body === undefined) {
        return res
            .status(404)
            .json({ success: false, message: "Something went wrong" });
    }
    // for validation
    const schema = Joi.object().keys({
        email_id: Joi.string()
            .email({ minDomainAtoms: 2 }).regex(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
            .required(),
        otp: Joi.number().required()
    });
    Joi.validate(req.body, schema, (err: any, value: string) => {
        if (err) {
            return res
                .status(400)
                .json({
                    err,
                    code: 400,
                    message: "joi validation error",
                    error_message: err
                });
        } else {
            // for password encryption
            var passwordarr = [
                "Aardvark",
                "Albatross",
                "Alligator",
                "Alpaca",
                "Ant",
                "Anteater",
                "Antelope",
                "Ape",
                "Armadillo",
                "Donkey",
                "Baboon",
                "Badger",
                "Barracuda",
                "Bat",
                "Bear",
                "Beaver",
                "Bee",
                "Bison",
                "Boar",
                "Buffalo",
                "Butterfly",
                "Camel",
                "Capybara",
                "Caribou",
                "Cassowary",
                "Cat",
                "Caterpillitems",
                "Cattle",
                "Chamois",
                "Cheetah",
                "Chicken",
                "Chimpanzee",
                "Chinchilla",
                "Chough",
                "Clam",
                "Cobra",
                "Cockroach",
                "Cod",
                "Cormorant",
                "Coyote",
                "Crab",
                "Crane",
                "Crocodile",
                "Crow",
                "Curlew",
                "Deer",
                "Dinosaur",
                "Dog",
                "Dogfish",
                "Dolphin",
                "Dotterel",
                "Dove",
                "Dragonfly",
                "Duck",
                "Dugong",
                "Dunlin",
                "Eagle",
                "Echidna",
                "Eel",
                "Eland",
                "Elephant",
                "Elk",
                "Emu",
                "Falcon",
                "Ferret",
                "Finch",
                "Fish",
                "Flamingo",
                "Fly",
                "Fox",
                "Frog",
                "Gaur",
                "Gazelle",
                "Gerbil",
                "Giraffe",
                "Gnat",
                "Gnu",
                "Goat",
                "Goldfinch",
                "Goldfish",
                "Goose",
                "Gorilla",
                "Goshawk",
                "Grasshopper",
                "Grouse",
                "Guanaco",
                "Gull",
                "Hamster",
                "Hare",
                "Hawk",
                "Hedgehog",
                "Heron",
                "Herring",
                "Hippopotamus",
                "Hornet",
                "Horse",
                "Human",
                "Hummingbird",
                "Hyena",
                "Ibex",
                "Ibis",
                "Jackal",
                "Jaguar",
                "Jay",
                "Jellyfish",
                "Kangaroo",
                "Kingfisher",
                "Koala",
                "Kookabura",
                "Kouprey",
                "Kudu",
                "Lapwing",
                "Lark",
                "Lemur",
                "Leopard",
                "Lion",
                "Llama",
                "Lobster",
                "Locust",
                "Loris",
                "Louse",
                "Lyrebird",
                "Magpie",
                "Mallard",
                "Manatee",
                "Mandrill",
                "Mantis",
                "Marten",
                "Meerkat",
                "Mink",
                "Mole",
                "Mongoose",
                "Monkey",
                "Moose",
                "Mosquito",
                "Mouse",
                "Mule",
                "Narwhal",
                "Newt",
                "Nightingale",
                "Octopus",
                "Okapi",
                "Opossum",
                "Oryx",
                "Ostrich",
                "Otter",
                "Owl",
                "Oyster",
                "Panther",
                "Parrot",
                "Partridge",
                "Peafowl",
                "Pelican",
                "Penguin",
                "Pheasant",
                "Pig",
                "Pigeon",
                "Pony",
                "Porcupine",
                "Porpoise",
                "Quail",
                "Quelea",
                "Quetzal",
                "Rabbit",
                "Raccoon",
                "Rail",
                "Ram",
                "Rat",
                "Raven",
                "Red deer",
                "Red panda",
                "Reindeer",
                "Rhinoceros",
                "Rook",
                "Salamander",
                "Salmon",
                "Sand Dollar",
                "Sandpiper",
                "Sardine",
                "Scorpion",
                "Seahorse",
                "Seal",
                "Shark",
                "Sheep",
                "Shrew",
                "Skunk",
                "Snail",
                "Snake",
                "Sparrow",
                "Spider",
                "Spoonbill",
                "Squid",
                "Squirrel",
                "Starling",
                "Stingray",
                "Stinkbug",
                "Stork",
                "Swallow",
                "Swan",
                "Tapir",
                "Tarsier",
                "Termite",
                "Tiger",
                "Toad",
                "Trout",
                "Turkey",
                "Turtle",
                "Viper",
                "Vulture",
                "Wallaby",
                "Walrus",
                "Wasp",
                "Weasel",
                "Whale",
                "Wildcat",
                "Wolf",
                "Wolverine",
                "Wombat",
                "Woodcock",
                "Woodpecker",
                "Worm",
                "Wren",
                "Yak",
                "Zebra"
            ]
            var password = passwordarr[Math.floor(Math.random() * passwordarr.length)] + 123;
            const saltRounds = 10
            const myPlaintextPassword = password
            const salt = bcrypt.genSaltSync(saltRounds)
            const passwordHash = bcrypt.hashSync(myPlaintextPassword, salt)
            let output = `
<h3>Hello user your new password details are mentioned below:</h3>
<ul> 
<li>email id : ${req.body.email_id}</li> 
  <li>password : ${password}</li>
  <li>For login<button><a href="www.google.com">Visit here</a></button></li>
</ul>`
            ForgotpassModel.findOne({ email_id: req.body.email_id, otp: req.body.otp })
                .then(result => {
                    if (result == null) {
                        return res.status(200).json({ status_code: 404, success: false, message: "Please correct otp and try again!!", error_message: err })
                    }
                    else {
                        CustomerModel.updateOne({email_id: req.body.email_id}, {
                            password: passwordHash
                        })
                        .then(result => {
                            ForgotpassModel.deleteOne({ email_id: req.body.email_id })
                                .then(result => {
                                    sendEmail({
                                        from: 'naveenpatel0202@gmail.com',
                                        to: req.body.email_id,
                                        subject: 'New password',
                                        html: output,
                                    })
                                    res.status(200).json({ status_code: 200, success: true, message: "new password will send on register email ID", result: result });
                                })
                                .catch(err => {
                                    res
                                        .status(202)
                                        .json({
                                            success: false,
                                            status_code: 404,
                                            message: "Something went wrong",
                                            error_message: err
                                        });
                                });
                        })
                        .catch(err => {
                            res
                                .status(202)
                                .json({
                                    success: false,
                                    status_code: 404,
                                    message: "Something went wrong",
                                    error_message: err
                                });
                        });
                    }
                })
                .catch(err => {
                    res.status(200).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
                })

        }
    })
}
export default setPassword;
