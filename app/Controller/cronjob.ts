// for user dashboard information
import ScheduleModel from "../Model/schedule";
import { Request, Response } from "express";
import CustomerModel from "../Model/customer";
import sendmail from 'sendmail'
/**
* @returns all product details
*/
const cronJob = () => {
    const emailSender = sendmail({
        silent: false
    });

    const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
        new Promise((resolve, reject) => {
            emailSender(options, (err, reply) => {
                // if error happened or returned code is now started with 2**
                if (err || !reply.startsWith("2")) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
        
        
    ScheduleModel.remove({})
        .then(results => {
            console.log("database truncate")
            CustomerModel.find()
                .then(result => {
                    var userDataCron: any = result
                    console.log(userDataCron)
                    var randomSender: any = [];
                    while (randomSender.length < userDataCron.length) {
                        var newSenderData = Math.floor(Math.random() * userDataCron.length);
                        if (randomSender.indexOf(newSenderData) === -1) randomSender.push(newSenderData)
                    }
                    for (var i = 0; i < randomSender.length; i++) {
                        var randomUsers: any = [];
                        var selectedSender: any = randomSender[i];
                        while (randomUsers.length < 3) {
                            var newRec: any = Math.floor(Math.random() * userDataCron.length)
                            if (randomUsers.indexOf(newRec) === -1 && newRec !== selectedSender) {
                                randomUsers.push(newRec);
                            }
                        }
                        var no: any = userDataCron[selectedSender]._id
                        var emailSendName: any = userDataCron[selectedSender].name
                        var emailSend: any = userDataCron[selectedSender].email_id
                        console.log("email",emailSend)
                        for (var k = 0; k < randomUsers.length; k++) {

                            var senderId: any = userDataCron[randomUsers[k]]._id
                            var senderName: any = userDataCron[randomUsers[k]].name
                            var senderProfileImage: any = userDataCron[randomUsers[k]].profile_photo
                            console.log(no,senderId,senderName,senderProfileImage)
                            let newData = new ScheduleModel({
                                sender_id: no,
                                receiver_id: senderId,
                                name: senderName,
                                profile_photo: senderProfileImage
                            });
                            newData
                                .save()
                                .then(result=>{
                                    console.log("data inserted")
                                })
                        }
                        var output = `
                            <h3>Hi ${emailSendName} please give feedback for :</h3>
                            <ul>
                            <li>Name:${userDataCron[randomUsers[0]].name}</li>
                            <li>Name:${userDataCron[randomUsers[1]].name}</li>
                            <li>Name:${userDataCron[randomUsers[2]].name}</li>
                            <li><a href="http://10.0.100.221:4030">Visit here</a></li>
                            </ul>`
                        sendEmail({
                            from: 'naveen.patel@neosofttech.com',
                            to: "naveenpatel0202@gmail.com",
                            subject: 'User login details',
                            html: output,
                        })
                        console.log("email send")

                    }


                })
                // res.status(200).json({ status_code: 200, success: true, message: "cron job list updated"})
                return console.log("cron job list updated")
        })

        .catch(err => {
            //res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default cronJob