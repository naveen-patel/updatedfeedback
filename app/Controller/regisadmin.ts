import AdminModel from "../Model/admin";
import { Request, Response } from "express";

const adminregis= (req:Request, res:Response) =>{
    if (req.body === undefined) {
        return res.status(404).json({ success: false, message: "Something went wrong" });
      }
      else{
        let newData = new AdminModel({
            name:req.body.name,
            passkey:req.body.passkey
           });
           newData
             .save()
             .then(result => {
                res.status(200).json({status_code:200,success: true,message: "Admin Successfully Registered"});
             })
             .catch(err=>{
                return res.status(404).json({ success: false, message: "Something went wrong" });
             })
      }
}
export default adminregis