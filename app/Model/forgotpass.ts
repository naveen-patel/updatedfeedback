import mongoose from 'mongoose'
const Schema = mongoose.Schema

const forgotpass = new Schema({
    customer_id: {
        type: String,
        ref: "registration"
    },
    email_id:{
        type:String,
        required:true,
        unique:true
    },
    otp:{
        type:String,
        required:true
    }
})

const ForgotpassModel = mongoose.model('forgotpass',forgotpass)

export default ForgotpassModel