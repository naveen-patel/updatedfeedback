import mongoose from 'mongoose'
const Schema = mongoose.Schema

const feedback = new Schema({
    customer_id: {
        type: String,
        ref: "registration"
    },
    feedback:{
        type:String,
        required:true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

const FeedbackModel = mongoose.model('feedback',feedback)

export default FeedbackModel