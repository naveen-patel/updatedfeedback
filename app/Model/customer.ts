import mongoose from 'mongoose'
const Schema = mongoose.Schema

const registration = new Schema({
    name:{
        type:String,
        required:true,
    },
    email_id:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true,     
    },
    profile_photo:{
        type:String,
        required:true  
    },
    registered_by:{
        type:String
    }
})

const CustomerModel = mongoose.model('registration',registration)

export default CustomerModel