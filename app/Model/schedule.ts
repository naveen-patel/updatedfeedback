import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schedule = new Schema({
    sender_id: {
        type: String,
        ref: "registration"
    },
    receiver_id: {
        type: String,
        ref: "registration"
    },
    name:{
        type:String,
        required:true
    },
    profile_photo:{
        type:String,
        required:true
        
    }, 
    flag:{
        type: Boolean,
        required: true,
        default: false
    }
    
})

const ScheduleModel = mongoose.model('schedule',schedule)

export default ScheduleModel