import mongoose from 'mongoose'
const Schema = mongoose.Schema

const admin = new Schema({
    name:{
        type:String,
        required:true,
    },
    passkey:{
        type:String,
        required:true,
        unique:true
    }
})

const AdminModel = mongoose.model('admin',admin)

export default AdminModel