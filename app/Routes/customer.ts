import newCustomer from '../Controller/registration'
import customerLogin from '../Controller/login'
import verifyToken from '../config/token';
import upload from '../config/mutler'
import dashboard from '../Controller/dashboard';
import feedbacksave from '../Controller/feedbacksave';
import cronJob from '../Controller/cronjob';
import receiver from '../Controller/receiver';
import cron from "node-cron";
import verify from '../Controller/verify';
import adminregis from '../Controller/regisadmin';
import user_registration from '../Controller/devregis';
import forgotPassword from '../Controller/forgotPassword';
import setPassword from '../Controller/setPassword';

// cron.schedule('*/2 * * * *', () => {
//     cronJob();
//     });
export class Customer {

    public routes(app: any): void {
        app.route('/users')
            .post(upload.single("profile_photo"), newCustomer)

        app.route('/login')
            .post(customerLogin)

        app.route('/dashboard')
            .get(verifyToken, dashboard)

        app.route('/feedbacksave')
            .post(verifyToken, feedbacksave)

        app.route('/cronJob')
            .get(cronJob)

        app.route('/receiver')
            .get(verifyToken, receiver)

        app.route('/verify')
            .post(verify)

        app.route('/adminregis')
            .post(adminregis)

        app.route('/forgotPassword')
            .post(forgotPassword)

        app.route('/setPassword')
            .post(setPassword)

        app.route('/user_registration')
            .post(upload.single("profile_photo"), user_registration)
    }
}